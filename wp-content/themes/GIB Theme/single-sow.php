<?php get_header();

if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
}



echo get_the_title();

add_main_photo(); 

?>
<div class="row">
<?php


$loop = new WP_Query(array('post_type' => 'sow', 'post_parent' => $post->ID));
if ($loop->have_posts()) :
  while ($loop->have_posts()) : $loop->the_post(); ?>
    <div class="col-3">

      <?php if (has_post_thumbnail()) { ?>
   
          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
     
      <?php } ?>
      <a href="<?php echo get_page_link(); ?>">
      <div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
        <div class="my-3 py-3">
          <h2 class="display-5"><?php echo get_the_title(); ?></h2>
          <?php add_main_photo(); ?>

          <a class="btn btn-outline-secondary" href="<?php echo get_page_link(); ?>"> ZOBACZ </a>
          <?php echo get_the_content(); ?>

          </p>
          
        </div>
        
        <div class="bg-light shadow-sm mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
      
      </div>
      
   



    
    </div>
        
<?php 




endwhile;

endif;
wp_reset_postdata(); 



?>
   </div>
      
   

<div id="carouselExampleIndicators" class="carousel slide img-thumbnail" data-ride="carousel" >
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
                    <?php 




$images = get_field('roller_color', $post->ID);
if( $images ): ?>


<div class="carousel-item active">
      <img class="d-block w-100" src="http://localhost/gib/wp-content/uploads/2019/11/Z%C5%82oty-d%C4%85b.jpg" class="img-fluid" alt="First slide">
    </div>
 
 <?php foreach( $images as $image ): ?>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo esc_url($image['sizes']['large']); ?>" class="img-fluid" alt="<?php echo esc_attr($image['alt']); ?>">
    </div>


    <?php endforeach; ?>
 
 <?php endif;
 
 ?>
 

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>










<!-- 
<?php

// LISTA

$images = get_field('roller_color', $post->ID);
if( $images ): ?>
    <ul>
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo esc_url($image['url']); ?>">
                     <img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                </a>
                <p><?php echo esc_html($image['caption']); ?></p>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?> -->






<script>// <![CDATA[
function goBack() { window.history.back() }
// ]]></script>
<button onclick="goBack()">WSTECZ</button>
<?php
  get_footer(); ?>