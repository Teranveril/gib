<?php get_header();
?>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
  <div class="background-image">
    <div class="col-md-5 p-lg-5 mx-auto my-5">

      <h1 class="display-4 font-weight-normal"> <?php bloginfo('name') ?></h1>

      <p class="lead font-weight-normal">Montaż Rolet</p>

      <a class="btn btn-outline-secondary" href="<?php echo "" ?>">Sprawdz ceny</a>

    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
  </div>
  </div>



  <div class="container-fluid">
  <div class="row justify-content-center">




    <?php


    $loop = new WP_Query(array('post_type' => 'sow', 'post_parent' => 0));
    if ($loop->have_posts()) :
      while ($loop->have_posts()) : $loop->the_post(); ?>
  <div class="col-2 mx-3 shadow-lg p-3 mb-5 bg-dark rounded">

          <?php if (has_post_thumbnail()) { ?>
      
              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        
          <?php } ?>

          <a href="<?php echo get_page_link(); ?>">
 
              <h3><?php echo get_the_title(); ?></h2>

              <a class="btn btn-outline-secondary" href="<?php echo get_page_link(); ?>"> <?php echo get_the_content(); ?></a>
              <?php add_main_photo(); ?>

              </p>
           


            </div>
    <?php endwhile;

    endif;
    wp_reset_postdata();
    ?>







  </div>
  </div>






  <?php
  get_footer(); ?>