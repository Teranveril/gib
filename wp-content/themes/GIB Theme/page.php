<!-- <?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */ ?>



<?php get_header();
?>


<body 
<?php body_class(); ?>> 
<?php if( have_posts() ): ?> 
<?php while(have_posts()): the_post(); ?> 
<article <?php post_class();?>> 
<h1><?php the_title(); ?></h1> 
<?php the_content(); ?> 
</article> <?php endwhile; ?> 
<?php else: ?> No content 
<?php endif; ?> 
<?php wp_footer(); ?> 
</body> -->


<?php
  get_footer(); ?>
