<?php


add_action('after_setup_theme', 'wordup_menu');

function wordup_menu()
{
    register_nav_menu('primary', __('Primary Menu', 'wordup'));
}

add_action('widgets_init', 'wordup_widgets_init');

function wordup_widgets_init()
{
    register_sidebar(array(
        'name' => 'Main sidebar',
        'id' => 'main-sidebar',
        'description' => 'Main sidebar displayed on every page'
    ));
}

function bootstrapstarter_wp_setup()
{
    add_theme_support('title-tag');
}

add_action('after_setup_theme', 'bootstrapstarter_wp_setup');


function themebs_enqueue_styles()
{

    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('core', get_template_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'themebs_enqueue_styles');

function themebs_enqueue_scripts()
{
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.bundle.min.js', array('jquery'));
}
add_action('wp_enqueue_scripts', 'themebs_enqueue_scripts');


function your_theme_enqueue_scripts() {
    // all styles
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.css', array(), 20141119 );
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/css/style.css', array(), 20141119 );
    // all scripts
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '20120206', true );
}
add_action( 'wp_enqueue_scripts', 'your_theme_enqueue_scripts' );

















function register_navwalker()
{
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');

function create_posttype_sow()
{

    register_post_type(
        'sow',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Systen Oslon Wewnetrznych'),
                'singular_name' => __('Systen Oslon Wewnetrznych')
            ),
            'hierarchical'       => true,
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'sow'),
            'supports' => array(
                'page-attributes' /* This will show the post parent field */,
                'title',
                'editor',
                'something-else',
            ),
        )
    );
}
// Hooking up our function to theme setup
add_action('init', 'create_posttype_sow');



function create_roller_tax()
{
    register_taxonomy(
        'genre',
        'roller',
        array(
            'label' => __('Genre'),
            'rewrite' => array('slug' => 'genre'),
            'hierarchical' => true,
        )
    );
}
add_action('init', 'create_roller_tax');

function add_main_photo()
{

    $image = get_field('main_photo');
    if (!empty($image)) : ?>
        <img src="<?php echo esc_url($image['url']); ?>" class="img-fluid" alt="<?php echo esc_attr($image['alt']); ?>" />
    <?php endif;
    }

    function add_secondary_photo()
    {

        $image = get_field('secondary_photo');
        if (!empty($image)) : ?>
        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
<?php endif;
}


















