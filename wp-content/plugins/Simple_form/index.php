



<?php
/*
Plugin Name: AH_Simple_Form
Description: Formularz kontaktowy
*/
?>


<?php
require './class.php';
include './class_field.php';




$form = new Form('send_form.php', FORM::GET);

// 1 - name
// 2 - type
// 3 - value
// 4 - checked
// 5 - placeholder

$form->add_field(new Field('first_name', 'text', [ 'placeholder' => 'wpisz imie', 'required' => 'required', 'pattern' => 'hgjhghj']));
$form->add_field(new Field('last_name', 'text', [ 'placeholder' => 'wpisz nazwisko' ]));
$form->add_field(new Field('phone', 'text', [ 'placeholder' => 'wpisz numer telefonu' ]));
$form->add_field(new Field('email', 'text', [ 'placeholder' => 'podaj swoj email' ]));
$form->add_field(new Field('accept', 'radio', []));
$form->add_field(new Field('Miasto', 'select', [ 'krakow' => 'Kraków',  'bierun' => 'Bieruń' ,  'Oswicim' => 'Oświęcim', 'Gdansk' => 'Grańsk'  ]));
$form->add_field(new Field('submit', 'submit', [ 'value' => 'submit' ]));


$form->render();

?>
