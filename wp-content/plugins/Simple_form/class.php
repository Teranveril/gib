
<?php

class Form
{
    const GET = 'get';
    const POST = 'post';
    public $fields = [];

    public function __construct($action, $method)
    {
        $this->action = $action;
        $this->method = $method;
    }

    public function render()
    {

        include 'form.php';
        
    }

    public function add_field($field)
    {
        $this->fields[] = $field;
    }

}
