<?php
class Field
{
    public $name;
    public $type;
    public $value;
    public $checked;
    public $placeholder;
    public $required;
    public $select_args = [];

    public function __construct($name, $type, array $args)
    {
        $this->name = $name;
        $this->type = $type;
        $this->select_args = $args;

        foreach ($args as $key => $value)
            $this->$key = $value;
    }
    public function render()
    {



        if ($this->type !== "select") {
            include 'form_render.php';
        }
        if ($this->type == "select") {
            include 'form_render_select.php';
        }
        if ($this->required) {
             "$this->required";
        }
    }
}
