<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'GIB' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[osepj9Pqj5:i?]%NlA*#4ZM,j<&):68t@mt}]jOQV[|<,3efnMnO78*dWz4mlX?' );
define( 'SECURE_AUTH_KEY',  '9:6Q&:JNan3 IR4:]>DS*>RR`Ybap)x%:-Gxur??Dnz4RXmqL(,ctU1tklQQIB_T' );
define( 'LOGGED_IN_KEY',    'T&8tAO2R=&Cc2Bdx*s%PEN;0#-EbS%_eU~2E21u-#/o>^$JcuKFX%q:*ucO+{LCH' );
define( 'NONCE_KEY',        'cVcF[M=c6fe5p Sw96D*:8X Z*GByvyC$NQ~=,f& t:]?UO:Sb$W=BWiSb?x)@)D' );
define( 'AUTH_SALT',        '1KS??$Xz^z[_[FJLddYAdUIx|lR6JM#~:8ly`x.rb^Gk6xKnZgCPl.L`qW Y0T~(' );
define( 'SECURE_AUTH_SALT', '?l?r?$qu$8(2;78 iI qi(c7G|ALSZf5IrOJP60%$Oy#GO@]`3/icpei.@>.ym5?' );
define( 'LOGGED_IN_SALT',   'Inb-U0o_ )gZcm80H3F1WPV:9asaWtnk#65_]gNn(DXzKj])`y(yq@<{VtbYIy}6' );
define( 'NONCE_SALT',       'T[c$Wj{!3?<)C^LSusTOOynaJ*Qhu/meb9(4X?Y/]fNEji`]|UG:kx?xBpWR`b<;' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'WPGIB1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
